"module implementing our first class"
class Car:
    """
    our first class, modeling a car
    pep talk:
    encapsulation
    data hiding
    inheritance
    polymorphism
    """
    def __init__(self, speed=70, engine=None):
        self.speed = speed
        self.name = 'mycar'
        self.engine = engine
    def drive(self):
        print 'driving', self.speed
    def wash(self):
        print 'now its clean'

def main():
    c = Car()
    c2 = Car(80)
    c.drive()
    c2.drive()
    c.wash()

if __name__ == '__main__':
    main()
